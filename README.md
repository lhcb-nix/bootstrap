# Nix bootstrap script

This repository uses docker to build a tarball for nix which uses `/cvmfs/lhcbdev.cern.ch/nix` instead of `/nix`.

## Building

The image can the be built (optionally passing `--build-arg lhcb_nix_commit=...`) using:
```
docker build --build-arg user_id=$UID --build-arg group_id=1470 -t lhcb-nix-bootstrap:latest .
```

NOTE: Various packages fail to built when using the `devicemapper` storage driver.
This can be worked around by mounting `/cvmfs` as a volume.


## Details

In order to move `/nix` to `/cvmfs` nix must be built twice.
The first time build a version of nix which is installed in `/nix/store/...` but builds binaries inside `/cvmfs/lhcbdev.cern.ch/nix/store/`.
The second build then builds a version of nix which independent of `/nix/store`.
