FROM alpine:3.7

ARG user_id
ARG group_id
ARG lhcb_nix_commit=2.0-maintenance-lhcb

# Check for mandatory build arguments
RUN : "${user_id:?Build argument user_id needs to be set and non-empty.}" \
    : "${group_id:?Build argument group_id needs to be set and non-empty.}" \
    : "${lhcb_nix_commit:?Build argument lhcb_nix_commit needs to be set and non-empty.}"

# Enable HTTPS support in wget.
RUN apk add --no-cache openssl ca-certificates bash bzip2 curl
RUN addgroup -g ${group_id} nixgroup
RUN adduser -G nixgroup -u ${user_id} -D nixuser
RUN mkdir -m 0755 /nix && chown nixuser /nix
RUN mkdir -p -m 0755 /cvmfs/lhcbdev.cern.ch/nix && chown nixuser -R /cvmfs/

# Install nix
USER nixuser
ENV USER=nixuser
RUN sh -c 'curl https://nixos.org/nix/install | sh'
RUN sh /home/nixuser/.nix-profile/etc/profile.d/nix.sh
ENV NIX_PATH=nixpkgs=/home/nixuser/.nix-defexpr/channels/nixpkgs \
    NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt \
    PATH=/home/nixuser/.nix-profile/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Install something resembling an environment
RUN /home/nixuser/.nix-profile/bin/nix-env -i git bash less curl man which gnutar bzip2 coreutils gnused
ENV PATH=/home/nixuser/.nix-profile/bin

# Clone the lhcb fork of nix
WORKDIR /home/nixuser
RUN git clone https://:@gitlab.cern.ch:8443/lhcb-nix/nix.git && cd nix && git checkout ${lhcb_nix_commit}
WORKDIR /home/nixuser/nix

# Build and install nix for the first time so it uses cvmfs for the store
RUN nix build --no-link -f release.nix build.x86_64-linux
RUN nix-env -i $(nix eval --raw -f release.nix build.x86_64-linux.outPath)

# Sourceforge is currently down, workaround this using my mirror
RUN nix-prefetch-url https://chrisburr.me/sourceforge_mirror/e2fsprogs-1.43.8.tar.gz
RUN nix-prefetch-url https://chrisburr.me/sourceforge_mirror/docutils-0.14.tar.gz
RUN nix-prefetch-url https://chrisburr.me/sourceforge_mirror/expat-2.2.5.tar.bz2

# Build nix and it's tarball inside cvmfs
RUN nix build --no-link -f release.nix build.x86_64-linux
RUN nix build --no-link -f release.nix binaryTarball.x86_64-linux
