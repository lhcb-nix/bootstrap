#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

mkdir "nix-binary-tarballs/"
cp -r /cvmfs/lhcbdev.cern.ch/nix/store/*binary-tarball*/ nix-binary-tarballs
